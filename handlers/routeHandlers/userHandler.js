/*
Title: user handler
Description: user handler for route
Author: Mrinal
Date:03.04.2022
*/

//Dependency
const {validate} = require('../../Helpers/validation');

//scaffolding
const handler={};

handler.userHandler =(requestProperties, callback)=>{

    const acceptedMethod = ['get','post','put','patch','delete'];
    if(acceptedMethod.indexOf(requestProperties.method)> -1 ){
        
        handler._users[requestProperties.method](requestProperties, callback);
    }else{
        callback(405);
    }
};

handler._users = {};
handler._users.get = (requestProperties, callback)=>{
    console.log('user get request');
}
handler._users.post = (requestProperties, callback)=>{
    const name = validate(requestProperties.name);
    const phone = validate(requestProperties.phone);
    const email = validate(requestProperties.email);
    console.log(name,email,phone);

}
handler._users.update = (requestProperties, callback)=>{

}
handler._users.put = (requestProperties, callback)=>{

}
handler._users.delete = (requestProperties, callback)=>{

}

module.exports = handler;