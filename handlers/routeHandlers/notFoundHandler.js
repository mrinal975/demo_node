/*
Title: Not Found Handler
Description: Not Found Handler
Author: Mrinal
Date:03.04.2022
*/

const handler ={};

handler.notFoundHandler= (requestProperties, callback)=>{
    // console.log('not found handler');
    callback(500, {
        message:'not found'
    });
}

module.exports = handler;