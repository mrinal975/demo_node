/*
Title: Routes
Description: Routes for app
Author: Mrinal
Date:03.04.2022
*/

//dependencies
const {sampleHandler} = require('./handlers/routeHandlers/sampleHandler');
const {userHandler} = require('./handlers/routeHandlers/userHandler');

const routes = {
    sample : sampleHandler,
    user : userHandler,
}

module.exports = routes;