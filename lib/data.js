/*
Title: store data on file system
Description: store data on file system
Author: Mrinal
Date:03.04.2022
*/

//Dependency

const fs = require('fs');
const path = require('path');

const lib = {};

//base directroy of the data  folder
lib.basedir = path.join(__dirname,'/../data/');

lib.create = (dir, file, data, callback)=>{
    fs.open(lib.basedir+dir+'/'+file+'.json','wx', (err, fileDescriptor) => {
        if(!err && fileDescriptor){
            //convert data to string
            const stringData = JSON.stringify(data);

            //write data to file and then close it
            fs.writeFile(fileDescriptor, stringData, (err2) => {
                if(!err2){
                    fs.close(fileDescriptor, (err3) => {
                        if(!err3){
                            callback(false);
                        }
                        else{
                            callback('Error closing file');  
                        }

                    });
                }else{
                    callback('Error writing to new file',err2);
                }
            })
        }
        else{
            callback( err);
        }
    })
}

lib.read = (dir, file,callback)=>{
    fs.readFile(`${lib.basedir + dir}/${file}.json`,'utf8', (err, data) =>{
        callback(err, data);
    });
};

lib.update = (dir, file, data, callback)=>{
    fs.open(`${lib.basedir + dir}/${file}.json`,'r+', (err, fileDescriptor) =>{
        // callback(err, data);
        if(!err && fileDescriptor){
            //convert the data to string
            const stringData = JSON.stringify(data);

            //truncate the file
            fs.ftruncate(fileDescriptor, (err1)=>{
                if(!err1)
                {
                    //write the file and close it
                    fs.writeFile(fileDescriptor,stringData, (err2)=>{
                        if(!err2){
                            fs.close(fileDescriptor, (err3)=>{
                                if(!err3){
                                    callback(false);
                                }
                                else{
                                    callback(err3);
                                }
                            })

                        }else{
                            callback(err2)
                        }
                    })

                }else{
                    callback(err1);
                }
            })
        }
    });
};

lib.delete = (dir, file, callback)=>{
    fs.unlink(`${lib.basedir + dir}/${file}.json`, (err)=>{
        if(err){
            callback(err)
        }
        callback(false);
    });
}

module.exports = lib;

