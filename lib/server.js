/*
Title: server
Description: server
Author: Mrinal
Date:03.04.2022
*/

//Dependencies
const http = require('http');
const {handleReqRes} = require('../Helpers/handelerReqRes');
const  env = require('../Helpers/environment');

//scaffolding
server = {};

server.createServer = ()=>{
    const createServerVariable = http.createServer(server.handleReqRes);
    createServerVariable.listen(env.port ,()=>{
          console.log(`listening on port ${env.port}`);
    })
}

// handle request response
server.handleReqRes = handleReqRes;

server.init = ()=>{
    server.createServer();
}


module.exports = server;