/*
Title: Handle Request and Response
Description: Handle Request and Response
Author: Mrinal
Date:03.04.2022
*/

//Dependencies
const url = require('url');
const {StringDecoder} = require('string_decoder');
const {notFoundHandler} = require('../handlers/routeHandlers/notFoundHandler');
const routes = require('../routes');
const {parseJSON} = require('./utilies');

// Module scaffoding
const handler = {};

handler.handleReqRes = (req,res)=>{
    //request handling
    //get url and parse it
    const parseUrl  = url.parse(req.url,true);
    const path = parseUrl.pathname;
    const trimPath = path.replace(/^\/+|\/+$/g,'');
    const method = req.method.toLowerCase();
    const queryStringObject = parseUrl.query;
    const header =  req.headers;
    const requestProperties = {
        parseUrl,
        path,
        trimPath,
        method,
        queryStringObject,
        header
    };
    const decoder = new StringDecoder('utf8');
    let realData = '';

    const chosenHandler = routes[trimPath]??notFoundHandler;


    req.on('data',(buffer)=>{
          realData += decoder.write(buffer);
    });
    req.on('end',()=>{
          realData += decoder.end();
        //   console.log(realData);
        requestProperties.body = parseJSON(realData);
        chosenHandler(requestProperties,(statusCode, payload)=>{
            statusCode = typeof(statusCode)==='number'?statusCode:500;
            payload = typeof(payload)==='object'?payload:{};
    
            payload  = JSON.stringify(payload);
            res.writeHead(statusCode);
            res.end(payload);
    
        });
            //response handle
          res.end('hi');
    })
    

};

module.exports = handler;