/*
Title: Utility
Description: Import Utility
Author: Mrinal
Date:03.04.2022
*/

//module scaffolding
const utilities = {};

//parse json string to object
 utilities.parseJSON = (jsonString)=>{
     let output;
     try{
        output = JSON.parse(jsonString);
    }
    catch{
        output = {};
    }
    return output;
}
module.exports = utilities;