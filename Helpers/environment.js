/*
Title: environment
Description: environment related thing
Author: Mrinal
Date:03.04.2022
*/

//dependency

//module scaffolding

const environments ={};

environments.staging = {
    port:3001,
    envName:'staging'
}

environments.production = {
    port:4000,
    envName:'production'
}

//determine which enviromnent passed

const currentEnvironment  = typeof(process.env.NODE_ENV)==='string'?process.env.NODE_ENV:'staging'

//export curresponding environment object
const environmentToExport = environments[currentEnvironment]??environments.staging;

//export env
module.exports = environmentToExport;