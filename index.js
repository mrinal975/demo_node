/*
Title: application initial file
Description: initial application file to start
Author: Mrinal
Date: 03.04.2022
*/

// Dependency
const server  = require('./lib/server');
const worker = require('./lib/worker');

//app Object - module scffoding     
const app = {};

//testing create file system
// data.create('test','newFile',{name:"bangladesh",lan:'Bangla'},(err)=>{
//       console.log('error was',err);
// })

// //testing reading file system
// data.read('test','newFile',(err, data)=>{
//       console.log('error was',err);
// })

// //testing updating file system
// data.update('test','newFile',{name:"mrinal", title:"Mallik"},(err, data)=>{
//       console.log('error was',err);
// })

//testing update file system
// data.delete('test','newFile',(err, data)=>{
//       console.log('error was',err);
// })

app.init =()=>{
      //start server
      server.init();

      // start the worker
      worker.init();
}

app.init()